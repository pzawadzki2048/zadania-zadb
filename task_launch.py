import random, timeit, uuid
from celery import group
import run_worker

def compute_sequence(n):
    return run_worker.compute_sequence.s(n)

def make_request():
    return compute_sequence(random.randint(1, 10))



def requests_suite():
    merged = []
    results = [make_request() for i in xrange(10)]

    res = group(results)().get()
    for elem in res:
        merged += elem

    print merged
print 'Ten successive runs:',
for i in range(1, 11):
    print '%.2fs' % timeit.timeit(requests_suite, number=1)
    print