from celery import Celery
import requests

celery = Celery('p19selery')

class Config:
    BROKER_URL='amqp://194.29.175.242'
    CELERY_RESULT_BACKEND = 'cache'
    CELERY_CACHE_BACKEND = 'memcached://194.29.175.242:11211'
    CELERY_DEFAULT_QUEUE = 'p19selery'

celery.config_from_object(Config)


@celery.task
def compute_sequence(n):
    url = 'http://www.random.org/integers/'
    params = {'num': n, 'min': 1, 'max': 20, 'col': 1, 'base': 10, 'format': 'plain', 'rnd': 'new'}
    resp = requests.get(url, params=params).text
    arr = resp.split('\n')
    arr.pop()

    sorted = []

    for number in arr:
        n = int(number)
        sorted.append(n)
    sorted.sort()
    return sorted

if __name__ == '__main__':
    celery.start()